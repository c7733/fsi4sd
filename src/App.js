import React from "react";
import {HomePage} from './components/HomePage'
import {NavBar} from './components/NavBar';
import {Routes, Route} from "react-router-dom";
import {Footer} from "./components/Footer";
import {About} from "./components/About";
import {Organizing} from "./components/Organizing";
import {Scientific} from "./components/Scientific";
import {Accepted} from "./components/Accepted";
import {Dowload} from "./components/Dowload";
import {Contact} from "./components/Contact";
import {Partner} from "./components/Partner";

function App() {
  return (
    <div className="App">
        <NavBar/>
        <Routes>
            <Route path="/" element={<HomePage/>}/>
            <Route path="/about" element={<About/>}/>
            <Route path="/organizing" element={<Organizing/>}/>
            <Route path="/scientific" element={<Scientific/>}/>
            <Route path="/partner" element={<Partner/>}/>
            <Route path="/accepted" element={<Accepted/>}/>
            <Route path="/download" element={<Dowload/>}/>
            <Route path="/contact" element={<Contact/>}/>
        </Routes>
        <Footer/>
    </div>
  );
}

export default App;
