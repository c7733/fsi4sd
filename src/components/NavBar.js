import React from "react";
import logo from '../images/logo.png'
import {Link} from "react-router-dom";
import '../style/style.css'
import menu from '../images/icons8-hamburger-64.png'
export const NavBar = () =>{
    return (
        <nav>
            <div>
                <img src={logo} id="header_nav_log" alt="Logo" className='link'/>
            </div>
            <div id="header_nav_link">
                <Link className='link' to="/" >Home</Link>
                <Link className='link' to="/about">About FSI4SD</Link>
               {/* <Link to="#">Committee</Link>
                <ul>
                    <li>
                        <Link to="/organizing">Organizing committee</Link>
                    </li>
                    <li>
                        <Link to="/scientific">Scientific committee</Link>
                    </li>
                </ul>*/}
                <Link className='link' to="/partner">Our partners</Link>
                <Link className='link' to="/accepted">Accepted papers</Link>
                {/*<Link to="/download">Download the program</Link>*/}
                <Link className='link' to="/contact">Contact</Link>
                <Link className='link' to="/about" id="read_more">Call for paper</Link>
            </div>
            <div id="header_nav_actions" className='link'>
                <Link className='link' to="/contact">Register</Link>
            </div>
            <img src={menu} id="" alt="Menu hamburger" className='menu-hamburger link'/>
        </nav>
    )
}
