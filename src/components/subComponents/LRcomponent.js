import React from "react";
import '../../style/style.css'
import {Bounce, Fade} from "react-reveal";
import {Link} from "react-router-dom";

export const LRcomponent = ({ id, image, text, title, }) => {
    function textFormater(text){
        let sectionData;
        sectionData = text.map(({ id, info}) => (
            <Fade top >
                <span key={id} id="textAnim">{info}, </span>
            </Fade>
        ));
        return sectionData;
    }
    return(
        id%2 === 0 ?
            (
                <section className="section-info">
                    <div>
                        <h2>{title}</h2>
                        <Bounce left >
                            <img src={require('../../images/'+image)}   className="netflix_on_tv_section_img" alt={id+"Image"}/>
                        </Bounce>
                    </div>
                    <div>
                        <div id="content_text">{textFormater(text)}</div>
                    </div>
                </section>
            )
            :
            (
                <section className="section-info">
                    <div>
                        <h2>{title}</h2>
                        <div id="content_text">{textFormater(text)} ... <span id="read_more"><Link to="/about" >Read more</Link></span></div>
                    </div>
                    <div>
                        <Fade bottom >
                            <img src={require('../../images/'+image)}   className="netflix_on_tv_section_img" alt={id+"Image"}/>
                        </Fade>
                    </div>
                </section>
            )
    )
}
