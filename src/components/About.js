import React from "react";
import '../style/auther.css'
export const About = () =>{
    return (
        <div id="about_header_content">

            <div>
                <h2 id="title">INTRODUCTION</h2>
                <p id="content">
                    The Virtual University of Côte d'Ivoire (UVCI), in collaboration with the Network of Teachers and Researchers in Computer Science of Faso (RECIF) and the various universities of Burkina Faso and Côte d'Ivoire, the Ministry in charge of Higher Education, Scientific Research and Innovation of Burkina Faso and Côte d'Ivoire (MESRSI), and the Ministry in charge of the Digital Economy, Posts and Digital Transformation (MENPTD) of Burkina Faso and Côte d'Ivoire, is organizing the first International Conference on Future Information and Communication Technologies for Sustainable Development (FISD-2024).  This international conference is a framework for African integration and will be held in a rotating manner in African countries (Burkina Faso, Côte d'Ivoire, Benin, Togo, Mali, Senegal)
                    This conference will provide a framework for exchanges between teachers-researchers, researchers, experts, engineers, companies, students in computer science from different countries and on the various issues of research in Computer Science related to technological innovations and the digital economy for sustainable development.
                    This conference will propose a view on the innovative solutions and methods conceived during the research activities in computer science carried out in the different countries and impacting all the sectors of activities.
                </p>
            </div>
            <div>
                <h2 id="title">CONTEXT AND JUSTIFICATION</h2>
                <p id="content">
                    The digital transformation that Africa is witnessing inevitably demonstrates the interest in considering the digital sector as one of the essential levers of socio-economic development and structural transformation of countries. Information technology can be a springboard for the socio-economic development of Burkina Faso, following the example of countries such as Kenya, Ethiopia or Rwanda. In this respect, quality training and research in computer science are essential to have African expertise in the field and to be able to provide innovative solutions to the major concerns of growth and development of the various countries.
                    In Burkina Faso, there are three laboratories at the Nazi BONI University, the Joseph KI-ZERBO University and the Norbert ZONGO University that have research teams in computer science.
                    In Côte d'Ivoire, there are five higher education institutions: the University Félix Houphouët Boigny, the University Nangui Abrogoua, the Virtual University of Côte d'Ivoire, the Institut National Polytechnique Félix Houphouët Boigny and the Ecole Supérieur Africaine des TIC, which have research laboratories in computer science.
                    <br/><br/>
                    How can computer science research and technological innovations constitute a strong potential for growth and endogenous development? Such is the current concern of this conference which proposes to lead the reflection around this problematic.
                    Also, the digital environment and the use of certain technologies raise new challenges in terms of security, data protection and privacy. Vulnerabilities and security flaws are reported daily in the news. How to take into account these security issues in the design and implementation of innovative solutions for sustainable development?
                    Data are processed and used in various specific or transversal applications and in different socio-economic sectors. They allow to predict certain trends and to improve decision making. How can we rely on applications dealing with massive data to contribute to economic and social development?
                    This conference will therefore allow us to reflect on the exploitation of massive data technologies, the consideration of the security dimension in all technological solutions and innovations, including vulnerabilities related to technological tools as well as security mechanisms and policies.
                </p>
            </div>
            <div>
                <h2 id="title">GOAL OF THE CONFERENCE</h2>
                <p id="content">
                    The overall objective of the FISD 2024 is to provide a framework for exchanges and reflections between the various actors in the digital field, including teachers, researchers, doctoral students, students and structures of the digital ecosystem around emerging themes and innovative solutions for the development of a true digital economy. Some participants will also present innovative solutions for a better visibility of their work.
                    More specifically, these are:

                    <ul>
                        <li>
                            to motivate, encourage and support research in computer science in order to bring innovative solutions to major development problems taking into account the real needs of the populations;
                        </li>
                        <li>
                            to work towards setting up international collaborations on federating and multidisciplinary themes;
                        </li>
                        <li>
                            drive innovation through emerging technologies and data;
                        </li>
                        <li>
                            to promote university/company collaborations around innovative research projects;
                        </li>
                    </ul>
                </p>
            </div>
            <div>
                <h2 id="title">EXPECTED OUTCOMES</h2>
                <p id="content">
                    It is expected from this conference that:
                    <ul>
                        <li>
                            presentations of research results are made;
                        </li>
                        <li>
                            multidisciplinary research teams on emerging themes are set up;
                        </li>
                        <li>
                            research and development projects be proposed in collaboration with companies;
                        </li>
                        <li>
                            different possible sources of funding for research projects are identified;
                        </li>
                        <li>
                            publication of the selected articles in indexing databases.
                        </li>
                    </ul>
                </p>
            </div>
            <div>
                <h2 id="title">METHODOLOGY</h2>
                <p id="content">
                    This conference will include several activities such as a hackathon between universities,
                    research presentation sessions (papers on accepted articles), workshops, booths, exhibitions and panels with breaks for participants.
                    The papers will be presented in sessions dedicated to specific topics so that the
                    authors of the papers can get to know each other and discuss together at a high technical level to allow better feedback on their respective work. <br/> <br/>
                    <strong>All accepted and presented papers will be published in springer.</strong>
                </p>
            </div>
        </div>
    )
}
