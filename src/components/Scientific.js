import React from "react";
import '../style/auther.css'

export const Scientific = () =>{

    return (
        <div id="scientific_committee">
            <div id="info_container">
                <h2>Scientific committee</h2>
                <span> Dr Abdoulaye SERE (Université Nazi BONI, Burkina Faso)<br/> </span>
                <span> Dr Jean Serge Dimitri OUATTARA (Université Joseph KI-ZERBO, Burkina Faso)<br/> </span>
                <span> Dr Yaya TRAORE (Université Joseph KI-ZERBO, Burkina Faso)<br/> </span>
                <span> Dr Moustapha BIKIENGA (Université Norbert ZONGO, Burkina Faso)<br/> </span>
                <span> Dr Borlli Michel Jonas SOME (Université Nazi BONI, Burkina Faso)<br/> </span>
                <span> Dr Pasteur PODA (Université Nazi BONI)<br/> </span>
                <span> Dr Télesphore TIENDREBEOGO (Université Nazi BONI, Burkina Faso)<br/> </span>
                <span> Dr Koné Tiémoman (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire) <br/> </span>
                <span> Dr Kouamé Euloge (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
                <span> Dr Atiampo Kodjo Armand (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
                <span> Dr Anoh Nogbou Georges (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
            </div>
            <div id="info_container">
                <h2> .</h2>
                <span> Dr Adepo Joel Christian (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
                <span> Dr N’guessan Behou Gérard (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
                <span> Dr Bakouan Mamadou (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
                <span> Dr Semon Mande (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
                <span> Dr Lobo Laby Clément (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
                <span> Dr Amanzou Nogbou Andetchi Aubin (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
                <span> Dr Kouadio Brou Pascal (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
                <span> Dr Sasso Sidonie Calice (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
                <span> Dr Jofack Soking Valère (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
                <span> Dr Kouassi Arthur Évariste (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
                <span> Dr Achi Harrisson Thiziers (Université Virtuelle de Côte d’Ivoire, Côte d’Ivoire)<br/> </span>
            </div>
        </div>
    )
}
