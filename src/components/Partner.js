import React from "react";
import logoUVCI from '../logos/logo_unVci.png'


export const Partner = () => {
    return(
        <div id="contact_header_content">
            <div id="">
                <h2>Our partners</h2>
                <div>
                    <img src={logoUVCI}  className="" alt={"logo"}/>
                </div>
                <div></div>
            </div>
        </div>
    )
}
