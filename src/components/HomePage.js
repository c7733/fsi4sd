import React from "react";
import '../style/style.css'
import {LRcomponent} from "./subComponents/LRcomponent";
import {mainData} from "../data/MainData";
import {Bounce} from "react-reveal";
import logoUVCI from '../logos/logo_unVci.png'
import logoRECIF from '../logos/img_21.png'

export const HomePage = () => {
    function renderSection(){
        let sectionData;
        sectionData = mainData().map(({ id, image, text, title, }) => (
            <LRcomponent id={id} key={id} title={title} text={text} image={image}/>
        ));
        return sectionData;
    }
    return(
        <>
            <header>
                <div id="header_content">
                    <div className="logoOrganizing">
                        <img src={logoUVCI} alt="logoUVCI" id="logoUVCI"/>
                    </div>
                    <div id="conferenceText">
                        <Bounce left>
                            <h1> International Conference on the  Future of  ICT  for  Sustainable Developpement
                                (FISD-2024)
                            </h1>
                        </Bounce>
                        <Bounce right>
                            <p id="header_content_subtitle">Artificial intelligence, Computer vision IOT, Blochain, Augmented reality, Virtual reality</p>
                        </Bounce>
                    </div>
                    <div className="logoOrganizing">
                        <img src={logoRECIF} alt="logoRECIF"/>
                    </div>
                </div>
                <div id="overlay"></div>
            </header>
            <div id="body">
                <div id="benefits_container">
                    {renderSection()}
                </div>
            </div>
        </>
    )
}
