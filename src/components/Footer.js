import React from "react";
import {Link} from "react-router-dom";
import '../style/style.css'
export const Footer = () => {
    return(
        <footer>
            <div><Link to="">Des questions ? Contactez nous.</Link></div>
            <div id="footer_link">
                <ul>
                    <li>
                        <Link to="">FAD</Link>
                    </li>
                    <li>
                        <Link to="">Relations</Link>
                    </li>
                    <li>
                        <Link to="">Confidentialité</Link>
                    </li>
                    <li>
                        <Link to="">Teste de vitesse</Link>
                    </li>
                </ul>
                <ul>
                    <li>
                        <Link to="">Centre d'aide</Link>
                    </li>
                    <li>
                        <Link to="">Recrutement</Link>
                    </li>
                    <li>
                        <Link to="">Preference des cookies</Link>
                    </li>
                    <li>
                        <Link to="">Information légales</Link>
                    </li>
                </ul>
                <ul>
                    <li>
                        <Link to="">Compte</Link>
                    </li>
                    <li>
                        <Link to="">Mode de lecture</Link>
                    </li>
                    <li>
                        <Link to="">Mentions légales</Link>
                    </li>
                    <li>
                        <Link to="">Seulement </Link>
                    </li>
                </ul>
            </div>
        </footer>
    )
}
