export const mainData = () => {
    return (
        [
            {
                id: 1,
                title: "About us",
                text: [
                    {id: 1, info: "The overall objective of the FISD 2024 is to provide" +
                            " a framework for exchanges and reflections between the various " +
                            "actors in the digital field, including teachers, researchers, doctoral students," +
                            " students and structures of the digital ecosystem around emerging themes and innovative" +
                            " solutions for the development of a true digital economy. Some participants will also present" +
                            " innovative solutions for a better visibility of their work"}
                            ],
                image: 'pexels_about.jpg'
            },
            {
                id: 2,
                title: "Our topics",
                text: [
                    {id: 1, info: "Cyber-security"},
                    {id: 2, info: " Networks and Internet technologies"},
                    {id: 3, info: "Databases"},
                    {id: 4, info: "Cloud computing"},
                    {id: 5, info: "Artificial intelligence"},
                    {id: 6, info: "Machine Learning"},
                    {id: 7, info: "Data science"},
                    {id: 8, info: "Big data and applications"},
                    {id: 9, info: "Business Intelligence"},
                    {id: 10, info: "Data Mining"},
                    {id: 11, info: "Bio-informatics"},
                    {id: 12, info: "Distributed Computing"},
                    {id: 13, info: "Embedded systems"},
                    {id: 14, info: "Mobile computing and telecommunications"},
                    {id: 15, info: "IoT"},
                    {id: 16, info: "GIS"},
                    {id: 17, info: "..."}
                ],
                image: "topics.png"
            },
        ]
    );
}
