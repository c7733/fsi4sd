export const ScientificCommitee = () => {
    return (

        [
            {id: 1, name: "Dr Abdoulaye SERE ", university: "Université Nazi BONI", country: "Burkina Faso"},
            {
                id: 2,
                name: "Dr Jean Serge Dimitri OUATTARA ",
                university: "Université Joseph KI-ZERBO",
                country: "Burkina Faso"
            },
            {id: 3, name: "Dr Yaya TRAORE ", university: "Université Joseph KI-ZERBO", country: "Burkina Faso"},
            {
                id: 4,
                name: "Dr Moustapha BIKIENGA",
                university: "Université Norbert ZONGO",
                country: "Burkina Faso"
            },
            {
                id: 5,
                name: "Dr Borlli Michel Jonas SOME ",
                university: "Université Nazi BONI",
                country: "Burkina Faso"
            },
            {id: 6, name: "Dr Pasteur PODA ", university: "Université Nazi BONI", country: "Burkina Faso"},
            {
                id: 7,
                name: "Dr Télesphore TIENDREBEOGO ",
                university: "Université Nazi BONI",
                country: "Burkina Faso"
            },
            {
                id: 8,
                name: "Dr Koné Tiémoman ",
                university: "Université Virtuelle de Côte d’Ivoire",
                country: "Côte d’Ivoire"
            },
            {
                id: 9,
                name: "Dr Kouamé Euloge ",
                university: "Université Virtuelle de Côte d’Ivoire",
                country: "Côte d’Ivoire"
            },
            {
                id: 10,
                name: "Dr Atiampo Kodjo Armand ",
                university: "Université Virtuelle de Côte d’Ivoire",
                country: "Côte d’Ivoire"
            },
            {
                id: 11,
                name: "Dr Anoh Nogbou Georges ",
                university: "Université Virtuelle de Côte d’Ivoire",
                country: "Côte d’Ivoire"
            },
            {
                id: 12,
                name: "Dr Adepo Joel Christian",
                university: "Université Virtuelle de Côte d’Ivoire",
                country: "Côte d’Ivoire"
            },
            {
                id: 13,
                name: "Dr N’guessan Behou Gérard ",
                university: "Université Virtuelle de Côte d’Ivoire",
                country: "Côte d’Ivoire"
            },
        ],
            [
                {
                    id: 14,
                    name: "Dr Bakouan Mamadou",
                    university: "Université Virtuelle de Côte d’Ivoire",
                    country: "Côte d’Ivoire"
                },
                {
                    id: 15,
                    name: "Dr Bakouan Mamadou",
                    university: "Université Virtuelle de Côte d’Ivoire",
                    country: "Côte d’Ivoire"
                },
                {
                    id: 16,
                    name: "Dr Semon Mande ",
                    university: "Université Virtuelle de Côte d’Ivoire",
                    country: "Côte d’Ivoire"
                },
                {
                    id: 17,
                    name: "Dr Lobo Laby Clément ",
                    university: "Université Virtuelle de Côte d’Ivoire",
                    country: "Côte d’Ivoire"
                },
                {
                    id: 18,
                    name: "Dr Amanzou Nogbou Andetchi Aubin ",
                    university: "Université Virtuelle de Côte d’Ivoire",
                    country: "Côte d’Ivoire"
                },
                {
                    id: 19,
                    name: "Dr Kouadio Brou Pascal ",
                    university: "Université Virtuelle de Côte d’Ivoire",
                    country: "Côte d’Ivoire"
                },
                {
                    id: 20,
                    name: "Dr Sasso Sidonie ",
                    university: "Université Virtuelle de Côte d’Ivoire",
                    country: "Côte d’Ivoire"
                },
                {
                    id: 21,
                    name: "Dr Jofack Soking Valère",
                    university: "Université Virtuelle de Côte d’Ivoire",
                    country: "Côte d’Ivoire"
                },
                {
                    id: 22,
                    name: "Dr Kouassi Arthur Évariste ",
                    university: "Université Virtuelle de Côte d’Ivoire",
                    country: "Côte d’Ivoire"
                },
                {
                    id: 23,
                    name: "Dr Achi Harrisson Thiziers  ",
                    university: "Université Virtuelle de Côte d’Ivoire",
                    country: "Côte d’Ivoire"
                },
            ]
    )
}
